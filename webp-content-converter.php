<?php
/**
 * Plugin Name: WebP Content Converter
 * Plugin URI: http://www.raccoon.ag
 * Description: This is the first version of this plugin. Initialy it will only convert all the img tags to amp-imgs, change the imgs src's to webp version, and add fallbacks to its original formats. This plugins does not work with Wordpress.com and dows not convert the images to webp, you have to use another plugin for this conversion. Future versions will do this all well.
 * Version: 2.0
 * Author: Leonardo Assef Coelho
 * Author URI: leonardo.coelho@raccoon.ag
 */

require 'vendor/autoload.php';

use WebPConvert\WebPConvert;

function add_webp_support( $mimes ) {
    $mimes['webp'] = 'image/webp';

   return $mimes;
}

add_filter( 'upload_mimes', 'add_webp_support' );

function get_attachment_id( $attachment_url) {

   global $wpdb;
   $attachment_id = false;

   // If there is no url, return.
   if ( '' == $attachment_url )
       return;

   // Get the upload directory paths
   $upload_dir_paths = wp_upload_dir();

   // Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
   if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {

       // If this is the URL of an auto-generated thumbnail, get the URL of the original image
       $attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

       // Remove the upload path base directory from the attachment URL
       $attachment_url = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );

       // Finally, run a custom database query to get the attachment ID from the modified attachment URL
       $attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_url ) );

       return $attachment_id;
   } else {
       return $attachment_id;
   }
}

function callback($matches) {
   $extensions = array('.jpeg', '.jpg', '.png');

   $link = $matches[3];
   $attachment_id = get_attachment_id($link);

   if ($attachment_id == false) {
       return '<img src="' . $matches[3] . '">';
   } else {

    $src = wp_get_attachment_image_src( $attachment_id, 'default');

    $img_width = $src[1];
    $img_heigth = $src[2];
 
     if(substr($link, -4) == '.jpg' || substr($link, -4) == '.png'):
         $img_webp =  substr($link, 0, -4) . ".webp";    
     elseif(substr($link, -5) == '.jpeg'):
         $img_webp =  substr($link, 0, -5) . ".webp";
     endif;
 
    $img_post = get_post($attachment_id);
    $img_title = $img_post->post_title;
    $img_srcset = wp_get_attachment_image_srcset($attachment_id);
 
    $alt_regex = '/alt=\"(.*?)[\"]/';
 
    $img_alt = preg_match($alt_regex, $matches[0]);
 
    $attachment_metadata = wp_get_attachment_metadata( $attachment_id );
 
    $image_basename = $attachment_metadata['sizes'];
 
    $srcset = '';
    $dirname = _wp_get_attachment_relative_path( $attachment_metadata['file'] );
    $upload_dir    = wp_get_upload_dir();
    $image_baseurl = trailingslashit( $upload_dir['baseurl'] ) . $dirname;
 
    foreach($image_basename as $value):
         if($value['width'] < 2048):
 
             $srcset_file = $value['file'];
 
             if(substr($srcset_file, -4) == '.jpg' || substr($srcset_file, -4) == '.png'):
                 $srcset_webp_file =  substr($srcset_file, 0, -4) . ".webp";    
             elseif(substr($srcset_file, -5) == '.jpeg'):
                 $srcset_webp_file =  substr($teste, 0, -5) . ".webp";
             endif;
 
             $srcset_webp = $srcset_webp . $image_baseurl . "/" . $srcset_webp_file . " " . $value['width'] . "w, ";
             $srcset_default = $srcset_default . $image_baseurl . "/" . $srcset_file . " " . $value['width'] . "w, ";
 
         endif;
    endforeach;
 
    $srcset_webp = rtrim($srcset_webp, ", ");
    $srcset_default = rtrim($srcset_default, ", ");
 
    // return $new_content = '<amp-img src="' . $img_webp . '" height="' . $img_heigth . '" width="' . $img_width . '" alt="' . $img_alt . '" title="' . $img_title . '" srcset="' . $srcset_webp . '" layout="intrinsic"><amp-img fallback src="' . $link . '" height="' . $img_heigth . '" width="' . $img_width . '" alt="' . $img_alt . '" title="' . $img_title . '" srcset="' . $srcset_default . '" layout="intrinsic"></amp-img></amp-img>';

    return $new_content = '<amp-img src="' . $img_webp . '" height="' . $img_heigth . '" width="' . $img_width . '" alt="' . $img_alt . '" title="' . $img_title . '" layout="intrinsic"><amp-img fallback src="' . $link . '" height="' . $img_heigth . '" width="' . $img_width . '" alt="' . $img_alt . '" title="' . $img_title . '" layout="intrinsic"></amp-img></amp-img>';
   }
}

function webp_content_replace($content) {

   $regex = '/<img([^>]+?)(\D*)src=\"(.*?)[\"][^>]+>/';

   $new_content = preg_replace_callback($regex, 'callback', $content);

   return $new_content;
}

add_filter( 'the_content', 'webp_content_replace');

function upload_convertion($file, $overrides = false, $time = null) {
    $source = $file['file'];
    $extensions = array('.jpeg', '.jpg', '.png');

    if(substr($source, -4) == '.jpg' || substr($source, -4) == '.png'):
        $destination =  substr($source, 0, -4) . ".webp";    
    elseif(substr($source, -5) == '.jpeg'):
        $destination =  substr($source, 0, -5) . ".webp";
    endif;

    $options = [];
    WebPConvert::convert($source, $destination, $options);

    return $file;
}
 
function crop_convertion($filename) {
    $source = $filename;
    $extensions = array('.jpeg', '.jpg', '.png');

    if(substr($source, -4) == '.jpg' || substr($source, -4) == '.png'):
        $destination =  substr($source, 0, -4) . ".webp";    
    elseif(substr($source, -5) == '.jpeg'):
        $destination =  substr($source, 0, -5) . ".webp";
    endif;

    $options = [];
    WebPConvert::convert($source, $destination, $options);

    return $filename;
}

function delete_webp($filename) {
    $extensions = array('.jpeg', '.jpg', '.png');

    foreach($extensions as $value) {
        if (strstr($filename, $value)) {
            $source = $filename;
        
            if(substr($source, -4) == '.jpg' || substr($source, -4) == '.png'):
                $newfilename =  substr($source, 0, -4) . ".webp";    
            elseif(substr($source, -5) == '.jpeg'):
                $newfilename =  substr($source, 0, -5) . ".webp";
            endif;
    
            unlink($newfilename);
            
        }
    }

    return $filename;
}

add_filter( 'image_make_intermediate_size', 'crop_convertion' , 10, 1 );
add_filter( 'wp_handle_upload', 'upload_convertion', 10, 2 );
add_filter( 'wp_delete_file', 'delete_webp', 10, 2 );
?>