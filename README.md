# WebP-Content-Converter

Replaces all img tags to amp-img's, adding the main image with webp format and a fallback to it's original image format and converts all uploaded images (jpg and png) to webp formats, including the crops.